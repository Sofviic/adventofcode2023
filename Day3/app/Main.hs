module Main (main) where

type Matrix = [[Char]]
type Pos = (Int,Int)

main :: IO ()
main = readFile "Input.txt" >>= putStrLn . show . solve . lines
--
solve :: Matrix -> [Integer]
solve m = fmap (sum . ($ m)) [partNumbers, gearNumbers]

--- Part 2
gearNumbers :: Matrix -> [Integer]
gearNumbers m = do
                    pos <- gearLocs m
                    let adjs = gearAdjs m pos
                    guard $ length adjs == 2
                    [product adjs]

gearLocs :: Matrix -> [Pos]
gearLocs m = do 
                (r,row) <- zip [0..] . fmap (zip [0..]) $ m
                (c,val) <- row
                guard $ '*' == val
                [(c,r)]

-- Bodge: with a tinge of christmas wishful thinking, a gear won't be next to two equal but distinct numbers, right?
gearAdjs :: Matrix -> Pos -> [Integer]
gearAdjs m pos = nodup . catMaybes $ [trueNumberAt m p | p <- posBlobIndices pos 1, withinBounds m p, trueNumberAt m p /= Nothing]

-- not necessarily leftmost digit
trueNumberAt :: Matrix -> Pos -> Maybe Integer
trueNumberAt m pos = if isDigit $ indexAt m pos 
                        then if maybefalse (leftIs isDigit m pos)
                                then trueNumberAt m $ mapPair (pred,id) pos
                                else Just $ numberAt m pos
                        else Nothing

--- Part 1
partNumbers :: Matrix -> [Integer]
partNumbers m = fmap fst
                    . filter (\(_,adjs) -> any id [True | c <- adjs, isSymbol c])
                    . fmap (\(pos,num) -> (num,posBlob m pos $ len num)) 
                    . allNumbers $ m
                    where len = length . show

allNumbers :: Matrix -> [(Pos,Integer)]
allNumbers m = fmap (toSnd $ numberAt m) . allNumberLocs $ m

allNumberLocs :: Matrix -> [Pos]
allNumberLocs m = do 
                (r,row) <- zip [0..] . fmap (zip [0..]) $ m
                (c,val) <- row
                guard $ isDigit val
                guard $ (not.maybefalse) (leftIs isDigit m (c,r))
                [(c,r)]

posBlob :: Matrix -> Pos -> Int -> [Char]
posBlob m pos len = [indexAt m p | p <- posBlobIndices pos len, withinBounds m p]

posBlobIndices :: Pos -> Int -> [Pos]
posBlobIndices (c,r) len = [(x,y) | x <- [c-1..c+len], y <- [r-1..r+1]]

-- leftmost digit
numberAt :: Matrix -> Pos -> Integer
numberAt m (c,r) = read . takeWhile isDigit . drop c $ (m !! r)

type Offset = (Int -> Int, Int -> Int)

maybefalse :: Maybe Bool -> Bool
maybefalse = fromMaybe False

offsetIs :: Offset -> (Char -> Bool) -> Matrix -> Pos -> Maybe Bool
offsetIs offset f m pos = if withinBounds m lp
                            then Just . f . indexAt m $ lp
                            else Nothing
                            where 
                                lp = mapPair offset pos

leftIs :: (Char -> Bool) -> Matrix -> Pos -> Maybe Bool
leftIs = offsetIs (pred,id)

-- Assumes in bounds
indexAt :: Matrix -> Pos -> Char
indexAt m (c,r) = (m !! r) !! c

withinBounds :: Matrix -> Pos -> Bool
withinBounds m = boundedPair (0,0) (length . head $ m, length m)

guard :: Bool -> [()]
guard True  = [()]
guard False = []

isSymbol :: Char -> Bool
isSymbol = (`elem` symbols)

symbols :: [Char]
symbols = "@#*$+@=%/&-"
-- Alternativly,
-- symbols :: Matrix -> [Char]
-- symbols = nodup . filter isDigit . filter (/= '.') . mconcat

isDigit :: Char -> Bool
isDigit = (`elem` "0123456789")

nodup :: Eq a => [a] -> [a]
nodup []     = []
nodup (x:xs) = (if x `elem` xs then [] else [x]) ++ nodup xs

boundedPair :: (Ord a, Ord b) => (a,b) -> (a,b) -> (a,b) -> Bool
boundedPair (la,lb) (ga,gb) (a,b) = bounded la ga a && bounded lb gb b

bounded :: Ord a => a -> a -> a -> Bool
bounded l g a = l <= a && a < g

toSnd :: (a -> b) -> a -> (a,b)
toSnd f a = (a,f a)

mapPair :: (a -> b, c -> d) -> (a,c) -> (b,d)
mapPair (f,g) (a,c) = (f a, g c)

fromMaybe :: a -> Maybe a -> a
fromMaybe e Nothing     = e
fromMaybe _ (Just x)    = x

catMaybes :: [Maybe a] -> [a] 
catMaybes []            = []
catMaybes (Just x:xs)   = x : catMaybes xs
catMaybes (Nothing:xs)  = catMaybes xs

test :: Matrix
test = ["467..114..",
        "...*......",
        "..35..633.",
        "......#...",
        "617*......",
        ".....+.58.",
        "..592.....",
        "......755.",
        "...$.*....",
        ".664.598.."]
