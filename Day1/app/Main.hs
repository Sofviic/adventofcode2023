module Main (main) where

main :: IO ()
main = readFile "Input.txt" >>= putStrLn . show . solve . lines
--
solve :: [String] -> [Integer]
solve vals = fmap (sum . (<$> vals)) [getLineValue1, getLineValue2]

--- Part 2
getLineValue2 :: String -> Integer
getLineValue2 s = (fromMaybe 0 . findFirst' . clean . splitOn "0123456789" $ s) * 10 +
                  (fromMaybe 0 . findLast' . clean . splitOn "0123456789" $ s)

findFirst' :: [String] -> Maybe Integer
findFirst' p
            | isDigit (head . head $ p) = Just . read . head $ p
            | otherwise                 = case firstSpeltNumber . head $ p of
                                            Nothing -> findFirst' $ drop 1 p
                                            Just sn -> Just sn

findLast' :: [String] -> Maybe Integer
findLast' p
            | isDigit (head . last $ p) = Just . read . last $ p
            | otherwise                 = case lastSpeltNumber . last $ p of
                                            Nothing -> findLast' $ dropLast 1 p
                                            Just sn -> Just sn

firstSpeltNumber :: String -> Maybe Integer
firstSpeltNumber = headS . mconcat . fmap catMaybes . (fmap.fmap) unspell . substringsAscending

lastSpeltNumber :: String -> Maybe Integer
lastSpeltNumber = headS . mconcat . fmap catMaybes . (fmap.fmap) unspell . substringsDescending

substringsAscending :: [a] -> [[[a]]]
substringsAscending s = fmap (prefixes . (`drop` s)) [0..length s - 1]

substringsDescending :: [a] -> [[[a]]]
substringsDescending s = fmap (suffixes . (`takeLast` s)) [1..length s]

prefixes :: [a] -> [[a]]
prefixes s = fmap (`take` s) [1..length s]

suffixes :: [a] -> [[a]]
suffixes s = fmap (`dropLast` s) [0..length s - 1]

catMaybes :: [Maybe a] -> [a] 
catMaybes []            = []
catMaybes (Just x:xs)   = x : catMaybes xs
catMaybes (Nothing:xs)  = catMaybes xs

unspell :: String -> Maybe Integer
unspell s = indexOf digitSpelling s 1

digitSpelling :: [String]
digitSpelling = [
                "one",
                "two",
                "three",
                "four",
                "five",
                "six",
                "seven",
                "eight",
                "nine"
                ]

splitOn :: Eq a => [a] -> [a] -> [[a]]
splitOn _  []   = []
splitOn ds x    = 
                    [takeWhile (not . (`elem` ds)) x] ++ 
                    [first (`elem` ds) x] ++ 
                    splitOn ds (drop 1 $ dropWhile (not . (`elem` ds)) x)

clean :: [String] -> [String]
clean = filter (/= "")

first :: (a -> Bool) -> [a] -> [a]
first f = take 1 . filter f

indexOf :: Eq a => [a] -> a -> Integer -> Maybe Integer
indexOf []      _ _ = Nothing
indexOf (x:xs)  a n = if x == a
                        then Just n
                        else indexOf xs a (n+1)

headS :: [a] -> Maybe a
headS [] = Nothing
headS x  = Just . head $ x

dropLast :: Int -> [a] -> [a]
dropLast n = reverse . drop n . reverse

takeLast :: Int -> [a] -> [a]
takeLast n = reverse . take n . reverse

--- Part 1
getLineValue1 :: String -> Integer
getLineValue1 s = read $ 
                    [fromMaybe '0' (findFirst isDigit s)] ++ 
                    [fromMaybe '0' (findLast isDigit s)]

fromMaybe :: a -> Maybe a -> a
fromMaybe e Nothing     = e
fromMaybe _ (Just x)    = x

findFirst :: (a -> Bool) -> [a] -> Maybe a
findFirst _ []      = Nothing
findFirst f (x:xs)  = if f x
                        then Just x
                        else findFirst f xs

findLast :: (a -> Bool) -> [a] -> Maybe a
findLast f = findFirst f . reverse

isDigit :: Char -> Bool
isDigit = (`elem` "0123456789")
