module Main (main) where

main :: IO ()
main = readFile "Input.txt" >>= putStrLn . show . solve . lines
--
solve :: [String] -> [Int]
solve s = fmap ($ s) [product . fmap ways . parse1, ways . parse2]

--- Part 2
-- Slow but eh :p
parse2 :: [String] -> (TotalTime,Distance)
parse2 = pairify . fmap (read . filter (/= ' ') . mconcat . drop 1 . splitStringCleanOn ":") . take 2

pairify :: [a] -> (a,a)
pairify [x,y] = (x, y)


--- Part 1
type Ms = Integer
type Mm = Integer
type MS = Integer -- Mm / Ms ; I just realised this doesn't matter (& I don't think will matter for part 2)
type Time = Ms
type TotalTime = Time
type Speed = MS
type Distance = Mm

parse1 :: [String] -> [(TotalTime,Distance)]
parse1 = zipify . fmap (fmap read . drop 1 . splitStringCleanOn " ") . take 2

ways :: (TotalTime,Distance) -> Int
ways (t,d) = length . filter (> d) $ allPossibles t

allPossibles :: TotalTime -> [Distance]
allPossibles t = fmap (distance t) [0..t]

distance :: TotalTime -> Speed -> Distance
distance t n = (t - n) * n



zipify :: [[a]] -> [(a,a)]
zipify [x,y] = zip x y



trimWhiteSpace :: String -> String
trimWhiteSpace = dropWhile (== ' ') . reverse . dropWhile (== ' ') . reverse

splitStringCleanOn :: String -> String -> [String]
splitStringCleanOn = fmap trimWhiteSpace .#. splitCleanOn

splitCleanOn :: Eq a => [a] -> [a] -> [[a]]
splitCleanOn = filter (/= []) .#. splitCleanOn'

splitCleanOn' :: Eq a => [a] -> [a] -> [[a]]
splitCleanOn' _  []   = []
splitCleanOn' ds x    = 
                        [takeWhile (not . (`elem` ds)) x] ++
                        splitCleanOn' ds (drop 1 $ dropWhile (not . (`elem` ds)) x)

infixr 9 .#.
(.#.) :: (c -> d) -> (a -> b -> c) -> a -> b -> d
f .#. g = (f .) . g

