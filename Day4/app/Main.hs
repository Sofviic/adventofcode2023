module Main (main) where

type WinNumber = Int
type Amount = Int
type Card = (Int,WinNumber,Amount)
type CardPile = [String]

main :: IO ()
main = readFile "Input.txt" >>= putStrLn . show . solve . lines
--
solve :: [String] -> [Integer]
solve s = fmap ($ s) [sum . fmap totalPoints, toInteger . totalScratchCards]

--- Part 2
totalScratchCards :: CardPile -> Amount
totalScratchCards = flip processCards 0 . cardify

processCards :: [Card] -> Amount -> Amount
processCards cards total = if all (\(_,_,a) -> a == 0) cards
                            then total
                            else processCards copies newTotal
                            where
                                copies = (collapseCards . computeCopies) cards
                                newTotal = total + (sum . fmap (\(_,_,a) -> a)) cards

computeCopies :: [Card] -> [Card]
computeCopies cs = emptyCards cs ++ do
                    (i,w,a) <- cs
                    (\j -> (i+j,0,a)) <$> [1..w]

collapseCards :: [Card] -> [Card]
collapseCards []            = []
collapseCards ((i,w,a):cs)  = c : collapseCards otherCards
                                where
                                    c = (i,w,a + amountsThisIndex cs i)
                                    otherCards = filter (\(j,_,_) -> i /= j)cs
                                    amountsThisIndex :: [Card] -> Int -> Amount
                                    amountsThisIndex cards index = sum
                                                                    . fmap (\(_,_,b) -> b)
                                                                    . filter (\(j,_,_) -> index == j)
                                                                    $ cards

cardify :: CardPile -> [Card]
cardify cards = do
                    c <- cards
                    [(cardID c, winNumber c, 1)]

cardID :: String -> Int
cardID = read . last . words . head . splitStringCleanOn ":|"

emptyCards :: [Card] -> [Card]
emptyCards = fmap $ \(i,w,_) -> (i,w,0)

--- Part 1
totalPoints :: String -> Integer
totalPoints s = if winNumber s < 1
                    then 0
                    else 2^(winNumber s - 1)

winNumber :: String -> WinNumber
winNumber s = sum $ do
                w <- winningNumbers s
                g <- grantedNumbers s
                guard $ w == g
                return 1

winningNumbers :: String -> [Integer]
winningNumbers = fmap read . words . head . drop 1 . splitStringCleanOn ":|"

grantedNumbers :: String -> [Integer]
grantedNumbers = fmap read . words . head . drop 2 . splitStringCleanOn ":|"





guard :: Bool -> [()]
guard True  = [()]
guard False = []


trimWhiteSpace :: String -> String
trimWhiteSpace = dropWhile (== ' ') . reverse . dropWhile (== ' ') . reverse

splitStringCleanOn :: String -> String -> [String]
splitStringCleanOn = fmap trimWhiteSpace .#. splitCleanOn

splitCleanOn :: Eq a => [a] -> [a] -> [[a]]
splitCleanOn = filter (/= []) .#. splitCleanOn'

splitCleanOn' :: Eq a => [a] -> [a] -> [[a]]
splitCleanOn' _  []   = []
splitCleanOn' ds x    = 
                        [takeWhile (not . (`elem` ds)) x] ++
                        splitCleanOn' ds (drop 1 $ dropWhile (not . (`elem` ds)) x)

infixr 9 .#.
(.#.) :: (c -> d) -> (a -> b -> c) -> a -> b -> d
f .#. g = (f .) . g

test :: [String]
test = ["Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53",
        "Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19",
        "Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1",
        "Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83",
        "Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36",
        "Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11"]
