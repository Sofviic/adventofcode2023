module Main (main) where

main :: IO ()
main = readFile "Input.txt" >>= putStrLn . show . solve . lines
--
solve :: [String] -> [Integer]
solve = (<$> [totalWinnings, totalWinnings2]) . flip ($) . fmap parse

--- Part 2
type IRank = [Int]

jRank :: Hand -> IRank
jRank h = bop j . reverse . sortBy compare . countDistinct $ nj
            where 
                j = length . filter (== 'J') $ h
                nj = filter (/= 'J') $ h

bop :: Num a => a -> [a] -> [a]
bop n []     = [n]
bop n (x:xs) = n+x : xs

totalWinnings2 :: [Play] -> Integer
totalWinnings2 = sum . zipWith (*) [1..] . fmap snd . sortHands2

sortHands2 :: [Play] -> [Play]
sortHands2 = sortBy (\(a,_) (b,_) -> compareHands2 a b)

compareHands2 :: Hand -> Hand -> Ordering
compareHands2 a b = if jRank a /= jRank b
                    then compare (jRank a) (jRank b)
                    else compareCards2 a b

compareCards2 :: Hand -> Hand -> Ordering
compareCards2 []     []     = EQ
compareCards2 []     _      = LT
compareCards2 _      []     = GT
compareCards2 (a:as) (b:bs) = case compareCard2 a b of
                                EQ  -> compareCards2 as bs
                                neq -> neq

compareCard2 :: Char -> Char -> Ordering
compareCard2 a b = compare (rankCard2 a) (rankCard2 b)

rankCard2 :: Char -> Rank
rankCard2 = fromMaybe (-1) . indexOf ranking2

ranking2 :: String
ranking2 = reverse "AKQT98765432J"

--- Part 1
type Hand = String
type Play = (Hand,Integer)

parse :: String -> Play
parse s = (h,read b)
            where (h:b:_) = words s

totalWinnings :: [Play] -> Integer
totalWinnings = sum . zipWith (*) [1..] . fmap snd . sortHands

sortHands :: [Play] -> [Play]
sortHands = sortBy (\(a,_) (b,_) -> compareHands a b)

compareHands :: Hand -> Hand -> Ordering
compareHands a b = if rank a /= rank b
                    then compare (rank a) (rank b)
                    else compareCards a b

compareCards :: Hand -> Hand -> Ordering
compareCards []     []     = EQ
compareCards []     _      = LT
compareCards _      []     = GT
compareCards (a:as) (b:bs) = case compareCard a b of
                                EQ  -> compareCards as bs
                                neq -> neq

compareCard :: Char -> Char -> Ordering
compareCard a b = compare (rankCard a) (rankCard b)
-- compareCard = (. rankCard) . compare . rankCard  -- Not obvious imo; so added the points

type Rank = Int

rank :: Hand -> Rank
rank = rankify . reverse . sortBy compare . countDistinct

rankify :: [Int] -> Rank
rankify [5] = 7
rankify [4,1] = 6
rankify [3,2] = 5
rankify [3,1,1] = 4
rankify [2,2,1] = 3
rankify [2,1,1,1] = 2
rankify [1,1,1,1,1] = 1
rankify _ = 0

countDistinct :: Eq a => [a] -> [Int]
countDistinct l = fmap fst . nodupBy snd $ fmap (\e -> (count l e, e)) l

count :: Eq a => [a] -> a -> Int
count []     _ = 0
count (x:xs) a = onTrue (a == x) (+1) $ count xs a

rankCard :: Char -> Rank
rankCard = fromMaybe (-1) . indexOf ranking

ranking :: String
ranking = reverse "AKQJT98765432"

nodupBy :: Eq b => (a -> b) -> [a] -> [a]
nodupBy _ []     = []
nodupBy f (x:xs) = x : nodupBy f (filter ((/= f x) . f) xs)

sortBy :: (a -> a -> Ordering) -> [a] -> [a]
sortBy _    []     = []
sortBy comp (x:xs) = sortBy comp lesser ++ [x] ++ sortBy comp greater
                        where
                            lesser = filter ((/= GT) . flip comp x) xs
                            greater = filter ((== GT) . flip comp x) xs

fromMaybe :: a -> Maybe a -> a
fromMaybe e Nothing  = e
fromMaybe _ (Just x) = x

indexOf :: Eq a => [a] -> a -> Maybe Int
indexOf l e = indexOf' l e 0

indexOf' :: Eq a => [a] -> a -> Int -> Maybe Int
indexOf' []      _ _ = Nothing
indexOf' (x:xs)  a n = if x == a
                        then Just n
                        else indexOf' xs a (n+1)

onTrue :: Bool -> (a -> a) -> a -> a
onTrue cond f = if cond
                    then f
                    else id

test :: [String]
test = ["32T3K 765",
        "T55J5 684",
        "KK677 28 ",
        "KTJJT 220",
        "QQQJA 483"]
