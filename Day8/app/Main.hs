module Main (main) where

main :: IO ()
main = readFile "Input.txt" >>= putStrLn . show . solve . lines
--
solve :: [String] -> [Integer]
solve = (<$> [solve1,solve2]) . flip ($)

--- Part 2
solve2 :: [String] -> Integer
solve2 cmap = walk2 (parseNetwork cmap) (parseDirInst . head $ cmap)

walk2 :: [Node] -> [Direction] -> Integer
walk2 net = walk2' 0 (getNodesEndsIn net "A") net

walk2' :: Integer -> [Node] -> [Node] -> [Direction] -> Integer
walk2' n cur net (d:ds) = if all ((== 'Z') . last . fst) cur
                            then n
                            else walk2' (n+1) (fmap (\node -> step net node d) cur) net ds
walk2' _ _   _   _      = error "Directions should be an infinite list"


getNodesEndsIn :: [Node] -> String -> [Node]
getNodesEndsIn net s = filter ((== s) . drop 2 . fst) $ net

--- Part 1
solve1 :: [String] -> Integer
solve1 cmap = walk (parseNetwork cmap) (parseDirInst . head $ cmap)

data Direction = L | R deriving (Show,Read)
type Path = (String,String)
type Node = (String,Path)

parseDirInst :: String -> [Direction]
parseDirInst = cycle . fmap (read . return)

parseNetwork :: [String] -> [Node]
parseNetwork = fmap parseNode . filter (/= "") . drop 2

parseNode :: String -> Node
parseNode s = (n,(l,r))
                where
                    [n,"=",dl,dr] = words s
                    l = take 3 . drop 1 $ dl
                    r = take 3          $ dr

walk :: [Node] -> [Direction] -> Integer
walk net = walk' 0 (getNode net "AAA") net

walk' :: Integer -> Node -> [Node] -> [Direction] -> Integer
walk' n cur net (d:ds) = if fst cur == "ZZZ"
                            then n
                            else walk' (n+1) (step net cur d) net ds
walk' _ _   _   _      = error "Directions should be an infinite list"

step :: [Node] -> Node -> Direction -> Node
step net (_,(l,_)) L = getNode net l
step net (_,(_,r)) R = getNode net r

getNode :: [Node] -> String -> Node
getNode net s = head . filter ((== s) . fst) $ net

test :: Int -> [String]
test 1 = [  "RL",
            "",
            "AAA = (BBB, CCC)",
            "BBB = (DDD, EEE)",
            "CCC = (ZZZ, GGG)",
            "DDD = (DDD, DDD)",
            "EEE = (EEE, EEE)",
            "GGG = (GGG, GGG)",
            "ZZZ = (ZZZ, ZZZ)"]
test 2 = [  "LLR",
            "",
            "AAA = (BBB, BBB)",
            "BBB = (AAA, ZZZ)",
            "ZZZ = (ZZZ, ZZZ)"]
test 3 = [  "LR",
            "",
            "11A = (11B, XXX)",
            "11B = (XXX, 11Z)",
            "11Z = (11B, XXX)",
            "22A = (22B, XXX)",
            "22B = (22C, 22C)",
            "22C = (22Z, 22Z)",
            "22Z = (22B, 22B)",
            "XXX = (XXX, XXX)"]
test _ = []
