module Main (main) where

main :: IO ()
main = readFile "Input.txt" >>= putStrLn . show . solve . lines
--
solve :: [String] -> [Integer]
solve s = [minimum . fmap (fullMapping s) $ getSeeds1 s, 
            minRangePoint . fullRangeMapping s $ getSeeds2 s]

--- Part 2
type SeedRange = Bounds
type Range = [Bounds]
type RangeMapping = Range -> Range

getSeeds2 :: Almanac -> [SeedRange]
getSeeds2 = rangify . pairify . fmap read . drop 1 . splitStringCleanOn " " . head

getRangeMappings :: MappingCat -> Almanac -> RangeMapping
getRangeMappings mc = rangeMappings . fmap mappify . takeWhile (/= "") . drop 1 . dropWhile (not . elem mc . prefixes)

fullRangeMapping :: Almanac -> RangeMapping
fullRangeMapping al = rangeMapConcat $ fmap (`getRangeMappings` al) categoricalMappings

rangeMapConcat :: [RangeMapping] -> RangeMapping
rangeMapConcat = foldr (.) id . reverse

rangeMappings :: [(Bounds,Mapping)] -> RangeMapping
rangeMappings fs = mconcat . fmap (applyRangeMappings fs)

applyRangeMappings :: [(Bounds,Mapping)] -> Bounds -> Range
applyRangeMappings [] x = [x]
applyRangeMappings fs x = fmap (applyCorrectMapping fs) $ boundsCuts (fmap fst fs) x

cleanRanges :: Range -> Range
cleanRanges = filter (\(a,b) -> a <= b)

applyCorrectMapping :: [(Bounds,Mapping)] -> Bounds -> Bounds
applyCorrectMapping []          x = x
applyCorrectMapping ((b,f):fs)  x = if b `contains` x
                                        then mapBoth f x
                                        else applyCorrectMapping fs x

minRangePoint :: Range -> Integer
minRangePoint = minimum . fmap fst . cleanRanges

contains :: Bounds -> Bounds -> Bool
contains (a1,b1) (a2,b2) = a1 <= a2 && b1 >= b2

boundsCuts :: Range -> Bounds -> Range
boundsCuts []     = return
boundsCuts (c:cs) = mconcat . fmap (boundsCuts cs) . boundsCut c

boundsCut :: Bounds -> Bounds -> Range
boundsCut (fa,fb) b = if fa <= fb
                            then mconcat . fmap (cutBound fb) $ cutBound fa b
                            else [b]

cutBound :: Integer -> Bounds -> Range
cutBound n (a,b) = if within (a,b) n
                    then [(a,n),(n,b)]
                    else [(a,b)]

rangify :: Num a => [(a,a)] -> [(a,a)]
rangify = fmap (\(a,n) -> (a,a+n))

pairify :: [a] -> [(a,a)]
pairify []          = []
pairify (x:y:rest)  = (x,y) : pairify rest

mapBoth :: (a -> b) -> (a,a) -> (b,b)
mapBoth = join mapPair

mapPair :: (a -> b) -> (c -> d) -> (a,c) -> (b,d)
mapPair f g (a,c) = (f a, g c)

join :: Monad m => m (m a) -> m a
join = (>>= id)

--- Part 1
type Almanac = [String]
type Seed = Integer
type MappingCat = String
type Mapping = Integer -> Integer
type Bounds = (Integer,Integer)

getSeeds1 :: Almanac -> [Seed]
getSeeds1 = fmap read . drop 1 . splitStringCleanOn " " . head

getMappings :: MappingCat -> Almanac -> Mapping
getMappings mc = concatMaps . fmap mappify . takeWhile (/= "") . drop 1 . dropWhile (not . elem mc . prefixes)

fullMapping :: Almanac -> Mapping
fullMapping al = mapConcat $ fmap (`getMappings` al) categoricalMappings

mapConcat :: [Mapping] -> Mapping
mapConcat = foldr (.) id . reverse

concatMaps :: [(Bounds,Mapping)] -> Mapping
concatMaps []         x = x
concatMaps ((b,f):fs) x = if within b x
                            then f x
                            else concatMaps fs x

mappify :: String -> (Bounds,Mapping)
mappify s = ((src,src+len), (+ (dst-src)))
            where [dst,src,len] = fmap read . splitCleanOn " " $ s

categoricalMappings :: [MappingCat]
categoricalMappings = [
                        "seed-to-soil",
                        "soil-to-fertilizer",
                        "fertilizer-to-water",
                        "water-to-light",
                        "light-to-temperature",
                        "temperature-to-humidity",
                        "humidity-to-location"
                        ]





prefixes :: [a] -> [[a]]
prefixes s = fmap (`take` s) [1..length s]

within :: Ord a => (a,a) -> a -> Bool
within (a,b) x = a <= x && x < b

trimWhiteSpace :: String -> String
trimWhiteSpace = dropWhile (== ' ') . reverse . dropWhile (== ' ') . reverse

splitStringCleanOn :: String -> String -> [String]
splitStringCleanOn = fmap trimWhiteSpace .#. splitCleanOn

splitCleanOn :: Eq a => [a] -> [a] -> [[a]]
splitCleanOn = filter (/= []) .#. splitCleanOn'

splitCleanOn' :: Eq a => [a] -> [a] -> [[a]]
splitCleanOn' _  []   = []
splitCleanOn' ds x    = 
                        [takeWhile (not . (`elem` ds)) x] ++
                        splitCleanOn' ds (drop 1 $ dropWhile (not . (`elem` ds)) x)

infixr 9 .#.
(.#.) :: (c -> d) -> (a -> b -> c) -> a -> b -> d
f .#. g = (f .) . g

test :: Almanac
test = ["seeds: 79 14 55 13",
        "",
        "seed-to-soil map:",
        "50 98 2",
        "52 50 48",
        "",
        "soil-to-fertilizer map:",
        "0 15 37",
        "37 52 2",
        "39 0 15",
        "",
        "fertilizer-to-water map:",
        "49 53 8",
        "0 11 42",
        "42 0 7",
        "57 7 4",
        "",
        "water-to-light map:",
        "88 18 7",
        "18 25 70",
        "",
        "light-to-temperature map:",
        "45 77 23",
        "81 45 19",
        "68 64 13",
        "",
        "temperature-to-humidity map:",
        "0 69 1",
        "1 0 69",
        "",
        "humidity-to-location map:",
        "60 56 37",
        "56 93 4",
        ""]
