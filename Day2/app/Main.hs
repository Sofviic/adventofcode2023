module Main (main) where

type Game = (Integer, [Reveal])
type Reveal = (Integer,Integer,Integer) -- rgb

main :: IO ()
main = readFile "Input.txt" >>= putStrLn . show . solve . lines
--
solve :: [String] -> [Integer]
solve vals = fmap (sum . (<$> vals)) [getLineValue1, getLineValue2]

--- Part 2
getLineValue2 :: String -> Integer
getLineValue2 s = trifoldr (*) 1 $ minimumBag rvs
                    where (_,rvs) = parse s

minimumBag :: [Reveal] -> Reveal
minimumBag = foldr (trimap max) (0,0,0)

trifoldr :: (a -> b -> b) -> b -> (a,a,a) -> b
trifoldr f e (x,y,z) = f x . f y . f z $ e

trimap :: (a -> b -> c) -> (a,a,a) -> (b,b,b) -> (c,c,c)
trimap f (a1,a2,a3) (b1,b2,b3) = (f a1 b1, f a2 b2, f a3 b3)

--- Part 1
bag :: Reveal
bag = (12,13,14)

getLineValue1 :: String -> Integer
getLineValue1 s = if all (allGreaterOrEqual bag) rvs
                    then gid
                    else 0
                    where (gid,rvs) = parse s

parse :: String -> Game
parse s = (read gid, rvs)
            where
                (gid:reveals:_) = splitStringCleanOn ":" . dropWhile (/= ' ') $ s
                rvs = fmap parseReveal . splitStringCleanOn ";" $ reveals

parseReveal :: String -> Reveal
parseReveal = collapseReveal . fmap parseColour . splitStringCleanOn ","

data ColourCube = RCube Integer | GCube Integer | BCube Integer

collapseReveal :: [ColourCube] -> Reveal
collapseReveal = foldr merge (0,0,0)
                    where
                        merge :: ColourCube -> Reveal -> Reveal
                        merge (RCube n) (r,g,b) = (r+n,g  ,b  )
                        merge (GCube n) (r,g,b) = (r  ,g+n,b  )
                        merge (BCube n) (r,g,b) = (r  ,g  ,b+n)

parseColour :: String -> ColourCube
parseColour s = parseConstructor colour $ read amount
                where [amount,colour] = splitStringCleanOn " " s

parseConstructor :: String -> Integer -> ColourCube
parseConstructor "red"   = RCube
parseConstructor "green" = GCube
parseConstructor "blue"  = BCube
parseConstructor _       = undefined

trimWhiteSpace :: String -> String
trimWhiteSpace = dropWhile (== ' ') . reverse . dropWhile (== ' ') . reverse

splitStringCleanOn :: String -> String -> [String]
splitStringCleanOn = fmap trimWhiteSpace .#. splitCleanOn

splitCleanOn :: Eq a => [a] -> [a] -> [[a]]
splitCleanOn = filter (/= []) .#. splitCleanOn'

splitCleanOn' :: Eq a => [a] -> [a] -> [[a]]
splitCleanOn' _  []   = []
splitCleanOn' ds x    = 
                        [takeWhile (not . (`elem` ds)) x] ++
                        splitCleanOn' ds (drop 1 $ dropWhile (not . (`elem` ds)) x)

allGreaterOrEqual :: Ord a => (a,a,a) -> (a,a,a) -> Bool
allGreaterOrEqual (a1,a2,a3) (b1,b2,b3) = 
                                            a1 >= b1 &&
                                            a2 >= b2 &&
                                            a3 >= b3

infixr 9 .#.
(.#.) :: (c -> d) -> (a -> b -> c) -> a -> b -> d
f .#. g = (f .) . g
